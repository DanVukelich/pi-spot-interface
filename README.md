# Pi-Spot Interface
 A web UI for https://gitlab.com/DanVukelich/pi-spot.

A web frontend to send simple JS8Call messages.  Intended use is self-spotting for POTA/SOTA activations.