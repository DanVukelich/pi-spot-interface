function GetVncStatus(){
    var request = new XMLHttpRequest();
    request.open('GET', '/api/vnc');
    request.send();

    request.onload = async function () {
        var data = JSON.parse(this.response);
        if(data.status)
            document.getElementById('vnc-status').innerHTML = 'Debugging VNC is live on port 5900';
        else
            document.getElementById('vnc-status').innerHTML = 'Debugging VNC is not running';
    }
}

function ToggleVnc() {
    var request = new XMLHttpRequest();
    request.open('POST', '/api/vnc');
    request.send();

    request.onload = async function () {
	GetVncStatus();
    }
}


GetVncStatus();
setInterval(GetVncStatus, 500);
