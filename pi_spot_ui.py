from quart import Quart, Request, render_template, request
from src.js8_service import JS8Service
from src.xorg_services import XorgServices
import js8net
import time

class UiHandler():

    def __init__(self, js8, display):
        self.js8 = js8
        self.display = display
    
    async def get(self):

        status = 'is running'
        if not self.js8.is_alive():
            status = 'process died'

        return await render_template(
            'index.html',
            test_str='JS8Call ' + status,
            other_str = '1'
        )

class DisplayHandler():

    def __init__(self, display):
        self.display = display

    async def get_vnc(self):
        result = dict({
            'status': self.display.is_vnc_running()
        })
        return (result, 200)
        
    async def toggle_vnc(self):
        if not self.display.is_vnc_running():
            self.display.start_vnc('192.168.1.217')
            return ('', 204)
        else:
            self.display.stop_vnc()
            return ('', 204)

def startup() -> Quart:
    app = Quart(
        __name__,
        static_url_path='',
        static_folder='static/'
    )

    xorg = XorgServices()
    xorg.start_x()
    js8 = JS8Service(xorg)
    js8.start()
    js8.make_fullscreen()
    ui = UiHandler(js8, xorg)
    display = DisplayHandler(xorg)
    
    app.add_url_rule('/', endpoint='get_index', methods=['GET'], view_func=ui.get)
    app.add_url_rule('/api/vnc', endpoint='toggle_vnc', methods=['POST'], view_func=display.toggle_vnc)
    app.add_url_rule('/api/vnc', endpoint='vnc_status', methods=['GET'], view_func=display.get_vnc)
    return app


app = startup()
if __name__ == "__main__":
    app.run()
