import subprocess
import sys
import os
import shutil
import socket
import time

from src import env_vars


class JS8Service():

    def __init__(self, xorg_services):
        self.xorg_services = xorg_services
    
    def start(self):        
        try:
            os.remove('/tmp/JS8Call.lock')
        except FileNotFoundError:
            pass
        try:            
            shutil.rmtree('/tmp/JS8Call')
        except FileNotFoundError:
            pass
                
        print(env_vars.service_env, file=sys.stdout)
        
        self.js8 = subprocess.Popen(
            ['js8call'],
            bufsize=0,
            env=env_vars.service_env
        )

        print('Waiting for JS8Call socket to open', file=sys.stderr)
        
        while not self.__wait_startup():
            time.sleep(1)
        
        print('JS8Call is ready', file=sys.stderr)

    def make_fullscreen(self):
        windows = []
        print('Waiting for JS8Call window to render', file=sys.stderr)
        while len(windows) == 0:
            windows = self.xorg_services.find_windows('JS8Call')
            
        print(windows, file=sys.stderr)
        self.xorg_services.fullscreen_windows(windows)

    def __wait_startup(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            return sock.connect_ex(('127.0.0.1', 2442)) == 0
        
    def is_alive(self):
        return self.js8.poll() is None
            
