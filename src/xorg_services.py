import subprocess
import asyncio
import getpass
import sys
import os
import shutil

from src import env_vars

class XorgServices():

    display = ':1'
    x_resolution = '1920'
    y_resolution = '1080'
    resolution = x_resolution + 'x' + y_resolution + 'x16'

    def __init__(self):
        env_vars.service_env['DISPLAY'] = self.display
        self.xvfb = None
        self.vnc = None
    
    def start_x(self):        
        self.xvfb = subprocess.Popen(
            ['Xvfb', self.display, '-screen', '0', self.resolution],
            env=env_vars.service_env
        )

    def is_backend_running(self):
        return self.xvfb.poll() is None

    def find_windows(self, name):
        xdotool = subprocess.Popen(
            ['xdotool', 'search', '--onlyvisible', '--maxdepth', '1', '--name', name ],
            env=env_vars.service_env,
            stdout=subprocess.PIPE
        )
        return xdotool.stdout.readlines()

    def fullscreen_windows(self, windows):
        for window in windows:
            xdotool = subprocess.Popen(
                ['xdotool', 'windowmove', window, '0', '0', 'windowsize', window, self.x_resolution, self.y_resolution],
                env=env_vars.service_env
            )
        

    def start_vnc(self, bind_ip):
        self.vnc = subprocess.Popen(
            ['x11vnc', '-display', self.display, '-forever', '-nopw', '-quiet', '-listen', bind_ip, '-xkb'],
            env=env_vars.service_env
        )

    def stop_vnc(self):
        self.vnc.kill()

    def is_vnc_running(self):
        if self.vnc is None:
            return False;
        return self.vnc.poll() is None
        
            
