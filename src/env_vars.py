import getpass

home_dir = '/home/' + getpass.getuser()
    
service_env = dict({
    'HOME': home_dir,
    'XDG_CONFIG_HOME': home_dir + '/.config/',
    'XDG_DATA_HOME': home_dir + '/.local/share/',
    'XDG_CACHE_HOME': home_dir + '/.cache/'
})
